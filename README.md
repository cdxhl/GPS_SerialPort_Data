# GPS模块卫星电文解析NMEA-183

#### 介绍
单片机串口接收GPS模块输出的卫星电文，并提取UTC时间、速度、海拔高度信息。

#### 硬件连接

1.  GPS模块串口输出--->STC8G1K08A_RX(P30引脚)
2.  STC8G1K08A_SDA(P33引脚)<--->0.96寸OLED_SDA
3.  STC8G1K08A_SCL(P32引脚)<--->0.96寸OLED_SCL

#### 软件架构

1.  GPS模块持续发送的信息量较大。约每隔1s发送几百个字节。单片机串口采用中断方式接收。内部设置两个状态机stat和stat2，分别用于接收NMEA-183中的GPRMC和GPGGA语句。
2.  首先对电文中的'GPRMC'和'GPGGA'关键字进行识别。一旦匹配成功后，进入缓冲接收状态。接收过程中收到“LF”(0x0a)换行符，则结束接收，改变状态标志位end_flag/end_flag2
3.  主程序循环判断end_flag/end_flag2状态。当发现有效后，提取缓冲区buffer/buffer2中的字段。随后进行计算和组装。为提高处理效率，减少丢失串口数据帧的概率，对于接收大值与上次比较，相同则不作处理，也不进行oled显示刷新。
![输入图片说明](1.jpg) 
#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
